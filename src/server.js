import sirv from 'sirv'
import polka from 'polka'
import compression from 'compression'
import cors from 'cors'
import * as sapper from '@sapper/server'

const { PORT, NODE_ENV } = process.env
const dev = NODE_ENV === 'development'

// const cors = (req, res, next) => {
//   res.header('Access-Control-Allow-Origin', '*');
//   res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
//   res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
//   res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
//   next();
// }

const app = polka() // You can also use Express
  .use(
    compression({ threshold: 0 }),
    cors(),
    sirv('static', { dev }),
    sapper.middleware()
  )
  // .use(cors())
  // .use(cors)
  // .use((req, res, next) => {
  //   res.header('Access-Control-Allow-Origin', '*');
  //   res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  //   res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  //   res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  //   next();
  // })
  .listen(PORT, err => {
    if (err) console.log('error', err)
  })

export default app.handler
